trij
====

Estimating the likelihood of every 3-digit number having an associated story of that number of new covid-19 cases.

I was asked my thoughts about [this Facebook post](https://www.facebook.com/tim.jackson.3367/posts/3390396844305739). I hadn't seen it, it was just described to me. After some thought, and trying desperately avoid bias, my knee jerk reaction was this it was either close to impossible, and therefore interesting, or so highly probable that would obviously occur.

Then, I actually looked at the post, and it was marked as false information with a [partner organization's story](https://leadstories.com/hoax-alert/2020/06/fact-check-typing-3-digit-number-and-new-cases-in-google-does-not-prove-covid-is-hoax.html) justifying this as false information. It was interesting and convincing, but I wanted to think a bit more about the probability. Though I did okay in school in maths, it wasn't immediately apparent to me how to calculate this probability.

Then I realized that the sort of person that would spread a story like this probably wouldn't be convinced by math alone anyway, so I decided to actually generate data and see what happened. You know, throw it in the dryer on the fluff cycle and see what we scrape off the lint screen.

Core Question
-------------

What is the likelihood of a given 3-digit number appearing in a set of randomly generated numbers between 0 and some maximum number of new cases per day, where the set length is the number of articles regarding new cases reported for populations worldwide.

Method
------

I take the sum of agencies about which a report may be created including the number of new cases in a day, then take the product of this number, the number of news stories per location per day, and the number of days between the declaration of pandemic (2020-03-11) and the day of the Facebook post (2020-06-22).

Using this as the length of a list of numbers, I randomly generate numbers between 0 and a maximum, which I discuss in [#assumptions-and-limitations]. For every number between 100 and 999 inclusive, I increment a number in another list. This list is the total number of times each 3-digit number was generated.

From here, I calculate the mean number of times each 3-digit number appears in the generated list.

I then repeated this generation process 100 times, and took the mean of those outcomes.

Assumptions and Limitations
---------------------------

For population groups I use: 50,000 cities, 3141 USA counties, and 192 countries. Of course this doesn't include demographics or other geographic distributions (e.g. new cases among LGBTQ individuals or new cases within the Mandan, Hidatsa, Arikara Nation). This is a conservative estimate.

For the number of reporting news agencies per group, I used the conservative minimum one. For example, Stewartsville, MO may not have an online reporting organization, but at least one within St. Joseph or DeKalb county would report on new cases in Stewartsville.

For the maximum number of new cases to be reported daily per population I selected 100,000. This number is crazy high. Very few populations would be likely to report numbers even close to this high. Most reports are probably single or two digits. But I see no reason to weight this number since we want the most cautious estimate possible.

Here is a list of significant limitations to the data analysis:

- Max cases per population should be limited by mean population for a type.
- Generated new cases value should be weighted toward zero for a more accurate distribution.
- Number of cases generated should follow the measured values of actual covid-19 distribution.
- Number of generated new cases should lower as a population is infected over time.
- There aren't likely to be reports for 0 new cases often, but I don't see a clean way to offset for this. Ideally, with properly weighted distributions this wouldn't be an issue.

Findings
--------

Given all the above, each number between 100 and 999 inclusively appears on average __54.9__ times in news stories covering new cases during that time period.

Had this number been less than one, this would be an interesting observation, but... it is pretty clear that every number is likely to be found several times.

Extra
-----

I imagine there is a parallel bias here to that of [The Birthday Problem](https://en.wikipedia.org/wiki/Birthday_problem). i.e. Some things seem really unlikely when they are in fact, extremely likely.

So, I don't begrudge people for being shocked by this. Our logarithmic brains make things unclear sometimes. I hope this helps.

Feedback
--------

Me and Jon Snow know all the same things about statistics, epidemiology, and population estimation, so I am probably wrong in many ways.

Furthermore, although I am quite proud of my programming chops, those aren't meant to be on display here. It is just a quick and dirty math problem. So, I'd love feedback on the math, but if you want to comment on the syntax styling, _contributions welcome_.

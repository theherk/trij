package main

import (
	"fmt"
	"math/rand"
	"time"
)

const generations = 100

var (
	maxCases     = 100000 // maximum cases per group
	cities       = 50000  // approximate worldwide
	counties     = 3141   // within the USA
	countries    = 192    // worldwide
	pandemicDays = 103    // 2020-03-11 - 2020-06-22 fs

	// newsPerGroup is the number of news stories generated about a
	// given group for a single day.
	// For example, if a small town were
	// to have the nearby cities newspaper write a story about its new
	// cases each day, that would represent one. This is a really
	// difficult number to estimate, which is talked about in the docs.
	newsPerGroup = 1
)

func gen(news int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	c := make([]int, 900)
	for i := 0; i < news; i++ {
		x := r.Intn(maxCases)
		if x >= 100 && x <= 999 {
			c[x-100]++
		}
	}
	return c
}

func meanCaseAppearance(cases []int) float64 {
	var t int
	for _, v := range cases {
		t += v
	}
	return float64(t) / float64(len(cases))
}

func meanOfMeans(means []float64) float64 {
	var sum float64
	for _, v := range means {
		sum += v
	}
	return sum / float64(len(means))
}

func news(groups, newsPerGroup, days int) int {
	return groups * newsPerGroup * days
}

func main() {
	n := news(cities+counties+countries, newsPerGroup, pandemicDays)
	var means [generations]float64
	for i, _ := range means {
		means[i] = meanCaseAppearance(gen(n))
	}
	fmt.Println(meanOfMeans(means[:]))
}
